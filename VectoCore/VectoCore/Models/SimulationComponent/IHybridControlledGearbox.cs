﻿namespace TUGraz.VectoCore.Models.SimulationComponent
{
	public interface IHybridControlledGearbox
	{
		bool SwitchToNeutral { set; }
	}
}