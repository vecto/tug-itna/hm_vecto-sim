﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VECTO3GUI2020.Views.Multistage.AdditionalJobInfoViews
{
    /// <summary>
    /// Interaction logic for AdditionalJobInfoViewMultiStage.xaml
    /// </summary>
    public partial class AdditionalJobInfoViewMultiStage : UserControl
    {
        public AdditionalJobInfoViewMultiStage()
        {
            InitializeComponent();
        }
    }
}
