﻿Namespace Electrics

Public Interface IM2_AverageElectricalLoadDemand

    Function GetAveragePowerDemandAtAlternator() As Single
    Function GetAveragePowerAtCrankFromElectrics() As Single

End Interface

End Namespace


