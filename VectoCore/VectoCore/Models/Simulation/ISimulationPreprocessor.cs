﻿namespace TUGraz.VectoCore.Models.Simulation {
	public interface ISimulationPreprocessor
	{
		void RunPreprocessing();
	}
}
