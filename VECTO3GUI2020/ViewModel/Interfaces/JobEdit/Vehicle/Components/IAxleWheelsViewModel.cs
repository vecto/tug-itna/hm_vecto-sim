﻿using TUGraz.VectoCommon.InputData;

namespace VECTO3GUI2020.ViewModel.Interfaces.JobEdit.Vehicle.Components
{
    interface IAxleWheelsViewModel : IAxlesDeclarationInputData, IComponentViewModel
    {
    }
}
