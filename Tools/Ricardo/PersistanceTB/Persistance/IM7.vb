﻿
Imports VectoAuxiliaries.Electrics
Imports VectoAuxiliaries.Pneumatics
Imports VectoAuxiliaries.Hvac

Namespace DownstreamModules


Public Interface IM7


   ReadOnly Property SmartElectricalAndPneumaticAuxAltPowerGenAtCrank As Single
   ReadOnly Property SmartElectricalAndPneumaticAuxAirCompPowerGenAtCrank As Single
   ReadOnly property SmartElectricalOnlyAuxAltPowerGenAtCrank as single
   ReadOnly Property SmartPneumaticOnlyAuxAirCompPowerGenAtCrank As Single
   
End Interface


End Namespace


