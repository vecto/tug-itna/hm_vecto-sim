﻿using System.Xml.Linq;

namespace VECTO3GUI2020.Util.XML.Documents
{
    public interface IXMLDeclarationJobWriter
    {
        XDocument GetDocument();
    }
}
